/**
 * Create RegExp object for class manipulation
 * @param {string} className
 * @returns {RegExp}
 */
function createRegExp(className) {
    var reTemplate = '((\\s|^)class)';
    var reStr = reTemplate.replace('class', className);

    return new RegExp(reStr, 'i');
}

/**
 * Check class in element class list
 * @param {HTMLElement} el
 * @param {string} className
 * @returns {boolean}
 */
function hasClass(el, className) {
    var re = createRegExp(className);

    return re.test(el.className);
}

/**
 * Add new class to element class list
 * @param {HTMLElement} el
 * @param {string} newClass
 */
function addClass(el, newClass) {
    if (false === hasClass(el, newClass)) {
        el.className += ' ' + newClass;
    }
}

/**
 * Remove class by name from element class list
 * @param {HTMLElement} el
 * @param {string} className
 */
function removeClass(el, className) {
    var re = createRegExp(className);

    if (hasClass(el, className)) {
        el.className = el.className.replace(re, '');
    }
}

/**
 *
 * @param {string} url
 * @param requestCallback
 */
function loadJSON(url, requestCallback) {
    var xhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhrObj = new xhr();

    xhrObj.open('GET', url, true);
    xhrObj.send();

    xhrObj.onreadystatechange = function () {
        if (this.readyState !== 4) {
            return false;
        }

        if (this.status !== 200) {
            alert('houston we have a problem!');
            console.log(xhrObj);
            throw new Error('Unexpected server response!');
        }

        requestCallback(xhrObj.responseText);
    }
}